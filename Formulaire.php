<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="Formulaire.css">
    <title>Fomrulaire.php</title>
</head>

<?php
if (isset($_GET['deleteID'])){
    if (is_file($_GET['deleteID'])){
        unlink($_GET['deleteID']);
    }
}
?>
<body>
    <header>
         <div class="jumbotron jumbotron-fluid">
            <div class="container">
            <h1 class="display-4">Mes taches</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
            </div>
        </div>
    </header>
    <section>
        <form method='POST'>
            <div class="form-group">
                <label for="exampleInputEmail1">Ajouter une tache</label>
                <input name="text" type= "text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">Le contenue sera interprétté comme du code HTML</small>
            </div>
             <button type="submit" class="btn btn-primary">Ajouter une tache</button>
        </form>
    <?php

        if(isset($_POST["text"])){
            echo '<div class="alert alert-success" role="alert">
            Taches ajoutées
       </div>'; 
        }
        
        elseif(isset($_GET['deleteID'])){
            echo '<div class="alert alert-danger" role="alert">
            A simple danger alert—check it out!
        </div>';
        }
        else {
            echo ''; 
        }
        
    ?>
    <h2>Liste des taches (<?php echo count(glob('tache/*txt'));?>)<h2>
    </section>
    <footer class="row">
    <?php
         if (isset ($_POST["text"])){
            $text= $_POST["text"];
            $x=mt_rand(0,1000000);
            $fp = fopen('tache/test' . $x . '.txt', 'w');
            fwrite($fp,$text);
            fclose($fp);
         }
    ?>
    
    <?php
        
            foreach (glob("tache/*.txt") as $chemindaccès) {
                $lecture = fopen($chemindaccès, "r");
                $contenue = fread($lecture, filesize($chemindaccès));
                fclose($lecture);
                
             echo
            '<div class="card" style="width: 18rem;">
             <div class="card-body">
                 <h5 class="card-title">' . $contenue . '</h5>
                 <a href="Formulaire.php?deleteID='.$chemindaccès.'"class="tn btn-warning">Suppirmer la tache</a>
             </div>
            </div>';
        }
     ?>
    </footer>
</body>
</html>